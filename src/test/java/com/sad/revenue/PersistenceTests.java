package com.sad.revenue;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.sad.revenue.utility.Money;

@ContextConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
public class PersistenceTests {

	@PersistenceContext
	private EntityManager entityManager;

	@Test
	public void testTest() {
		assertEquals(1, 1);
	}

	// test for PRODUCT
	@Test
	@Transactional
	public void testProduct() throws Exception {
		Product product = new Product();
		product.setName("TestProduct 101");
		product.setType("test");
		entityManager.persist(product);
		assertNotNull(product.getId());
		// System.out.println(product.getId());
		entityManager.clear();

		Product other = (Product) entityManager.find(Product.class,
				product.getId());
		assertNotNull(other.getId());
		assertEquals(true, product.equals(other));
		entityManager.clear();
	}

	// test for CONTRACT
	@Test
	@Transactional
	public void testContract() throws Exception {
		Contract contract = new Contract();
		contract.setRevenue(new Money(99.99));
		entityManager.persist(contract);
		// entityManager.flush();
		// System.out.println(contract.getRevenue().amount());
		assertNotNull(contract.getId());
		assertNotNull(contract.getRevenue());

		Contract other = (Contract) entityManager.find(Contract.class,
				contract.getId());
		assertNotNull(other.getId());
		entityManager.clear();
	}

	// test for REVENUERECOGNITION
	@Test
	@Transactional
	public void testRevenueRecognition() throws Exception {
		RevenueRecognition rev = new RevenueRecognition();
		rev.setAmount(new Money(99.99));
		entityManager.persist(rev);
		assertNotNull(rev.getId());
		assertNotNull(rev.getAmount());

		RevenueRecognition other = (RevenueRecognition) entityManager.find(
				RevenueRecognition.class, rev.getId());
		assertNotNull(other.getId());
		entityManager.clear();

	}

	// @Test
	// @Transactional
	// public void testSaveOrderWithItems() throws Exception {
	// Order order = new Order();
	// order.getItems().add(new Item());
	// entityManager.persist(order);
	// entityManager.flush();
	// assertNotNull(order.getId());
	// }
	//
	// @Test
	// @Transactional
	// public void testSaveAndGet() throws Exception {
	// Order order = new Order();
	// order.getItems().add(new Item());
	// entityManager.persist(order);
	// entityManager.flush();
	// // Otherwise the query returns the existing order (and we didn't set the
	// // parent in the item)...
	// entityManager.clear();
	// Order other = (Order) entityManager.find(Order.class, order.getId());
	// assertEquals(1, other.getItems().size());
	// assertEquals(other, other.getItems().iterator().next().getOrder());
	// }
	//
	// @Test
	// @Transactional
	// public void testSaveAndFind() throws Exception {
	// Order order = new Order();
	// Item item = new Item();
	// item.setProduct("foo");
	// order.getItems().add(item);
	// entityManager.persist(order);
	// entityManager.flush();
	// // Otherwise the query returns the existing order (and we didn't set the
	// // parent in the item)...
	// entityManager.clear();
	// Order other = (Order) entityManager
	// .createQuery(
	// "select o from Order o join o.items i where i.product=:product")
	// .setParameter("product", "foo").getSingleResult();
	// assertEquals(1, other.getItems().size());
	// assertEquals(other, other.getItems().iterator().next().getOrder());
	// }

}
