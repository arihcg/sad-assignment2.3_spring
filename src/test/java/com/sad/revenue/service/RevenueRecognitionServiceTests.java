package com.sad.revenue.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.math.BigInteger;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.sad.revenue.Contract;
import com.sad.revenue.Product;
import com.sad.revenue.RevenueRecognition;
import com.sad.revenue.utility.MfDate;
import com.sad.revenue.utility.Money;

@ContextConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
public class RevenueRecognitionServiceTests {

	// @PersistenceContext
	// private EntityManager entityManager;

	@Autowired
	private ProductService productService;

	@Autowired
	private ContractService contractService;

	@Autowired
	private RevenueRecognitionService rrs;

	@Test
	public void testTest() {
		assertEquals(1, 1);
	}

	@Test
	public void testCalculateRevenueRecognitions() {
		Product word = new Product("TEST: Word Processor 2014", "W");
		Money revenue = new Money(2500.50);
		MfDate dateSigned = new MfDate(2014, 2, 8);
		Contract contract = new Contract(revenue, dateSigned, word);
		long contractID = contractService.insertContract(contract);
		assertNotNull("Contract is null", contractID);
		Money result = null;
		rrs.calculateRevenueRecognitions(contract);
		result = rrs.recognizedRevenue(contract, dateSigned);
//		System.out.println(result.amount().toString());
		assertEquals(new BigDecimal(new BigInteger("250050"), 2), result.amount());
	}
	
	// @Test
	// public void testProductService() {
	// Product product = new Product("new product", "new type");
	// long id = productService.insertProduct(product);
	// assertNotNull(id);
	//
	// Product other = productService.findProduct(id);
	// assertEquals("Persistent object not equal to original object.", product,
	// other);
	//
	// productService.deleteProduct(id);
	// Product another = productService.findProduct(id);
	// assertEquals(null, another);
	// }
	//
	// @Autowired
	// private ContractService contractService;
	//
	// @Test
	// public void testContractService(){
	// Product product = new Product("new product", "new type");
	// Contract contract = new Contract(new Money(99.99), new MfDate(2015, 2,
	// 8), product);
	//
	// long id = contractService.insertContract(contract);
	// assertNotNull(id);
	//
	// Contract other = contractService.findContract(id);
	// assertEquals(contract.getId(), other.getId());
	// assertEquals(product.getName(), other.getProduct().getName());
	// // System.out.println(other.getProduct().getName());
	// // System.out.println(other.getWhenSigned().toString());
	// }
	//

	// // test for PRODUCT
	// @Test
	// @Transactional
	// public void testProduct() throws Exception {
	// Product product = new Product();
	// product.setName("TestProduct 101");
	// product.setType("test");
	// entityManager.persist(product);
	// entityManager.flush();
	// assertNotNull(product.getId());
	// // System.out.println(product.getId());
	// entityManager.clear();
	//
	// Product other = (Product) entityManager.find(Product.class,
	// product.getId());
	// assertNotNull(other.getId());
	// assertEquals(true, product.equals(other));
	// entityManager.clear();
	// }
	//
	// // test for CONTRACT
	// @Test
	// @Transactional
	// public void testContract() throws Exception {
	// Contract contract = new Contract();
	// contract.setRevenue(new Money(99.99));
	// entityManager.persist(contract);
	// // entityManager.flush();
	// // System.out.println(contract.getRevenue().amount());
	// assertNotNull(contract.getId());
	// assertNotNull(contract.getRevenue());
	//
	// Contract other = (Contract) entityManager.find(Contract.class,
	// contract.getId());
	// assertNotNull(other.getId());
	// entityManager.clear();
	// }
	//
	// // test for REVENUERECOGNITION
	// @Test
	// @Transactional
	// public void testRevenueRecognition() throws Exception {
	// RevenueRecognition rev = new RevenueRecognition();
	// rev.setAmount(new Money(99.99));
	// entityManager.persist(rev);
	// assertNotNull(rev.getId());
	// assertNotNull(rev.getAmount());
	//
	// RevenueRecognition other = (RevenueRecognition) entityManager.find(
	// RevenueRecognition.class, rev.getId());
	// assertNotNull(other.getId());
	// entityManager.clear();
	//
	// }

	// @Test
	// @Transactional
	// public void testSaveOrderWithItems() throws Exception {
	// Order order = new Order();
	// order.getItems().add(new Item());
	// entityManager.persist(order);
	// entityManager.flush();
	// assertNotNull(order.getId());
	// }
	//
	// @Test
	// @Transactional
	// public void testSaveAndGet() throws Exception {
	// Order order = new Order();
	// order.getItems().add(new Item());
	// entityManager.persist(order);
	// entityManager.flush();
	// // Otherwise the query returns the existing order (and we didn't set the
	// // parent in the item)...
	// entityManager.clear();
	// Order other = (Order) entityManager.find(Order.class, order.getId());
	// assertEquals(1, other.getItems().size());
	// assertEquals(other, other.getItems().iterator().next().getOrder());
	// }
	//
	// @Test
	// @Transactional
	// public void testSaveAndFind() throws Exception {
	// Order order = new Order();
	// Item item = new Item();
	// item.setProduct("foo");
	// order.getItems().add(item);
	// entityManager.persist(order);
	// entityManager.flush();
	// // Otherwise the query returns the existing order (and we didn't set the
	// // parent in the item)...
	// entityManager.clear();
	// Order other = (Order) entityManager
	// .createQuery(
	// "select o from Order o join o.items i where i.product=:product")
	// .setParameter("product", "foo").getSingleResult();
	// assertEquals(1, other.getItems().size());
	// assertEquals(other, other.getItems().iterator().next().getOrder());
	// }

}
