package com.sad.revenue;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.sad.revenue.utility.MfDate;
import com.sad.revenue.utility.Money;

@Entity
public class Contract implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private long id;
	@OneToMany(cascade=CascadeType.ALL)
	private Collection<RevenueRecognition> revenueRecognitions = new ArrayList<RevenueRecognition>();
	@OneToOne(cascade=CascadeType.ALL)
	private Product product;
	@Embedded
	private Money revenue;
	@Embedded
	private MfDate whenSigned;

	public Contract() {
	}

	public Contract(Money revenue, MfDate dateSigned, Product product) {
		this.revenue = revenue;
		this.whenSigned = dateSigned;
		this.product = product;
	}

	public long getId() {
		return id;
	}

	public void addRevenueRecognition(RevenueRecognition rev) {
		revenueRecognitions.add(rev);
	}

	public Collection<RevenueRecognition> getRevenueRecognitions() {
		return revenueRecognitions;
	}

	public void setRevenueRecognitions(
			Collection<RevenueRecognition> revenueRecognitions) {
		this.revenueRecognitions = revenueRecognitions;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Money getRevenue() {
		return revenue;
	}

	public void setRevenue(Money revenue) {
		this.revenue = revenue;
	}

	public MfDate getWhenSigned() {
		return whenSigned;
	}

	public void setWhenSigned(MfDate whenSigned) {
		this.whenSigned = whenSigned;
	}

}
