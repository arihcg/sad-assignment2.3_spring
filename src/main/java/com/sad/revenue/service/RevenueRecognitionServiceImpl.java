package com.sad.revenue.service;

import java.util.Collection;
import java.util.Iterator;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sun.org.mozilla.javascript.internal.ast.SwitchCase;

import com.sad.revenue.Contract;
import com.sad.revenue.RevenueRecognition;
import com.sad.revenue.utility.MfDate;
import com.sad.revenue.utility.Money;

@Service
public class RevenueRecognitionServiceImpl implements RevenueRecognitionService {

	// @PersistenceContext
	// private EntityManager em;

	@Autowired
	private RevenueService rs;

	public Money recognizedRevenue(Contract contract, MfDate asOf) {
		Money result = Money.dollars(0);

		Collection<RevenueRecognition> revenueRecognitions = rs
				.findRevenueRecognitionsFor(contract);
//		System.out.println("SIZE: " + revenueRecognitions.size());
		Iterator<RevenueRecognition> it = revenueRecognitions.iterator();
		while (it.hasNext()) {
			RevenueRecognition rev = it.next();
			result = result.add(rev.getAmount());
		}
		return result;
	}

	public void calculateRevenueRecognitions(Contract contract) {
		// Contract contract = rs.findContract(contractNumber);
		Money totalRevenue = contract.getRevenue();
		MfDate recognitionDate = contract.getWhenSigned();
		String type = contract.getProduct().getType();

		if (type.equals("S")) {
			Money[] allocation = totalRevenue.allocate(3);
			rs.insertRecognition(contract, allocation[0], recognitionDate);
			rs.insertRecognition(contract, allocation[1],
					recognitionDate.addDays(60));
			rs.insertRecognition(contract, allocation[2],
					recognitionDate.addDays(90));
		} else if (type.equals("W")) {
			rs.insertRecognition(contract, totalRevenue, recognitionDate);
		} else if (type.equals("D")) {
			Money[] allocation = totalRevenue.allocate(3);
			rs.insertRecognition(contract, allocation[0], recognitionDate);
			rs.insertRecognition(contract, allocation[1],
					recognitionDate.addDays(60));
			rs.insertRecognition(contract, allocation[2],
					recognitionDate.addDays(90));
		}

	}

}
