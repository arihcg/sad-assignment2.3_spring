package com.sad.revenue.service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sad.revenue.Contract;
import com.sad.revenue.Product;
import com.sad.revenue.utility.MfDate;
import com.sad.revenue.utility.Money;

@Service
public class ContractServiceImpl implements ContractService {
	@PersistenceContext
	private EntityManager em;

	@Transactional
	public long insertContract(Money revenue, MfDate dateSigned, Product product) {
		return insertContract(new Contract(revenue, dateSigned, product));
	}

	@Transactional
	public long insertContract(Contract contract) {
//		em.persist(contract.getProduct());
		em.persist(contract);
		return contract.getId();
	}

	@Transactional(readOnly = true)
	public Contract findContract(long id) {
		Contract contract = em.find(Contract.class, id);
		return contract;
	}
	
	@Transactional
	public void deleteContract(long id) {
		Contract contract = findContract(id);
		em.remove(contract);
	}

}
