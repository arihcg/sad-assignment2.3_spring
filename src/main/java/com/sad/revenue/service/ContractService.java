package com.sad.revenue.service;

import com.sad.revenue.Contract;
import com.sad.revenue.Product;
import com.sad.revenue.utility.MfDate;
import com.sad.revenue.utility.Money;

public interface ContractService {
	public long insertContract(Money revenue, MfDate dateSigned, Product product);

	public long insertContract(Contract contract);

	public Contract findContract(long id);

	public void deleteContract(long id);

}
