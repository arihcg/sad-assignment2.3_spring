package com.sad.revenue.service;

import com.sad.revenue.Product;

public interface ProductService {
	public long insertProduct(String name, String type);
	
	public long insertProduct(Product product);
	
	public void deleteProduct(long id);
	
	public Product findProduct(long id);
	// public static long insertNewWordProcessor(String name);
	// public static long insertNewSpreadsheetProcessor(String name);
	// public static long insertNewDatabaseProcessor(String name);
}
