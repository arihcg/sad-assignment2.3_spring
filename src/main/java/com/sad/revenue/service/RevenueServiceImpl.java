package com.sad.revenue.service;

import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sad.revenue.Contract;
import com.sad.revenue.RevenueRecognition;
import com.sad.revenue.utility.MfDate;
import com.sad.revenue.utility.Money;

@Service
public class RevenueServiceImpl implements RevenueService{
	
	@PersistenceContext
	private EntityManager em;
	
	@Transactional(readOnly = true)
	public RevenueRecognition findRevenueRecognition(long id) {
		RevenueRecognition rev = em.find(RevenueRecognition.class, id);
		return rev;
	}
	@Transactional(readOnly = true)
	public Contract findContract(long id) {
		Contract contract = em.find(Contract.class, id);
		return contract;
	}
	
	@Transactional
	public long insertRecognition(Contract contract, Money revenue, MfDate on) {
		RevenueRecognition rev = new RevenueRecognition(contract, revenue, on);
		contract.addRevenueRecognition(rev);
		return insertRecognition(rev);
	}
	
	@Transactional
	public long insertRecognition(RevenueRecognition recognition) {
		em.persist(recognition);
		return recognition.getId();
	}
	
	@Transactional
	public void deleteRecognition(long id, MfDate date) {
		RevenueRecognition rev = findRevenueRecognition(id);
		em.remove(rev);
	}
	
	@Transactional(readOnly = true)
	public Collection<RevenueRecognition> findRevenueRecognitionsFor(long contractID) {
		Contract contract = em.find(Contract.class, contractID);
		return contract.getRevenueRecognitions();
	}
	
	@Transactional(readOnly = true)
	public Collection<RevenueRecognition> findRevenueRecognitionsFor(
			Contract contract) {
		return contract.getRevenueRecognitions();
	}
	

}
