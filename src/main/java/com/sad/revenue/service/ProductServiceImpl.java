package com.sad.revenue.service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sad.revenue.Product;

@Service
public class ProductServiceImpl implements ProductService {

	@PersistenceContext
	private EntityManager em;

	@Transactional
	public long insertProduct(Product product) {
		em.persist(product);
		return product.getId();
	}

	@Transactional
	public long insertProduct(String name, String type) {
		insertProduct(new Product(name, type));
		return 0;
	}

	@Transactional
	public void deleteProduct(long id) {
		Product product = findProduct(id);
		em.remove(product);
	}

	@Transactional(readOnly = true)
	public Product findProduct(long id) {
		Product product = em.find(Product.class, id);
		return product;
	}

	// public static long insertNewWordProcessor(String name) {
	// Product word = new Product(name, "W");
	// ProductServiceImpl ps = new ProductServiceImpl();
	// ps.insertProduct(word);
	// return word.getId();
	// }
	// public static long insertNewSpreadsheetProcessor(String name);
	// public static long insertNewDatabaseProcessor(String name);

}
