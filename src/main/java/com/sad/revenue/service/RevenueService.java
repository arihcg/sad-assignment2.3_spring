package com.sad.revenue.service;

import java.util.Collection;

import com.sad.revenue.Contract;
import com.sad.revenue.RevenueRecognition;
import com.sad.revenue.utility.MfDate;
import com.sad.revenue.utility.Money;

public interface RevenueService {

	public RevenueRecognition findRevenueRecognition(long id);

	public Collection<RevenueRecognition> findRevenueRecognitionsFor(
			long contractID);

	public Collection<RevenueRecognition> findRevenueRecognitionsFor(
			Contract contract);

	public Contract findContract(long id);

	public long insertRecognition(Contract contract, Money revenue, MfDate on);

	public long insertRecognition(RevenueRecognition recognition);

	public void deleteRecognition(long id, MfDate date);

}
