package com.sad.revenue.service;

import com.sad.revenue.Contract;
import com.sad.revenue.utility.MfDate;
import com.sad.revenue.utility.Money;

public interface RevenueRecognitionService {
	public Money recognizedRevenue(Contract contract, MfDate asOf);

	public void calculateRevenueRecognitions(Contract contract);
	
}
