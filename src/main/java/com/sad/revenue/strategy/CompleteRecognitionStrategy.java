package com.sad.revenue.strategy;

import com.sad.revenue.Contract;
import com.sad.revenue.RevenueRecognition;

public class CompleteRecognitionStrategy extends RecognitionStrategy {

	@Override
	public void calculateRevenueRecognitions(Contract contract) {
		contract.addRevenueRecognition(new RevenueRecognition(contract
				.getRevenue(), contract.getWhenSigned()));

	}

}
