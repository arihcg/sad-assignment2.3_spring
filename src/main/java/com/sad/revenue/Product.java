package com.sad.revenue;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Product implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	private long id;
	private String name;
	private String type;

	public Product() {
	}

	public Product(String name, String type) {
		this.name = name;
		this.type = type;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean equals(Object object) {
		if (object instanceof Product) {
			Product other = (Product) object;
			return this.name == other.getName() && this.type == other.getType();
			// return this.getName().equals(other.getName())
			// && this.getType().equals(other.getType());
		}
		return false;
	}

}
